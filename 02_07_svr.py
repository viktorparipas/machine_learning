# SVR

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('02_06_Position_Salaries.csv')
X = dataset.iloc[:, 1:2].values
y = dataset.iloc[:, 2].values

# Splitting the dataset into the Training set and Test set
"""from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)"""

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_y = StandardScaler()
X = sc_X.fit_transform(X)   # fits and transforms (scales) original array, converts to float
y = y.reshape((len(y),1))   # reshapes y vector to 2D array
y = sc_y.fit_transform(y)   # fits and transforms (scales) original array, converts to float
y = y.ravel()               # flattens array

# Fitting SVR to the dataset
from sklearn.svm import SVR
regressor = SVR(kernel = 'rbf', C = 5.0)
regressor.fit(X, y)

# Predicting a new result
# temp = sc_X.transform(np.array([[6.5]]))
# y_pred = regressor.predict(temp)
# y_pred = regressor.predict(sc_X.transform(np.array([[6.5]])))
temp = np.array([[6.5]])    # converts scalar to 1x1 array
# transform method does not return anything, modifies sc_X in place
# predict value at scaled 6.5 --> scaled
# inverse transform to get unscaled value
y_pred = sc_y.inverse_transform(regressor.predict(sc_X.transform(temp)))

# Visualising the SVR results
plt.scatter(X, y, color = 'red')
plt.plot(X, regressor.predict(X), color = 'blue')
plt.title('Truth or Bluff (SVR)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()

# Visualising the SVR results (for higher resolution and smoother curve)
X_grid = np.arange(min(X), max(X), 0.01) # choice of 0.01 instead of 0.1 step because the data is feature scaled
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(X, y, color = 'red')
plt.plot(X_grid, regressor.predict(X_grid), color = 'blue')
plt.title('Truth or Bluff (SVR)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()