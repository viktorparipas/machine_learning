# Upper Confidence Bound

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('06_32_Ads_CTR_Optimisation.csv')

# Implementing UCB
import math
N = 10000                           # Number of users/rounds
d = 10                              # Number of ads
ads_selected = []                   # List of selected ads for each user
numbers_of_selections = [0] * d     # Number of times each ad were selected
sums_of_rewards = [0] * d           # Sum of rewards for each ad
total_reward = 0
for n in range(N):                  # iterating through users/rounds
    ad = 0
    max_upper_bound = 0             # starting upper bound for each user
    for i in range(d):              # iterating through ads
        if (numbers_of_selections[i] > 0):
            # Average reward for each ad: R_i / N_i
            average_reward = sums_of_rewards[i] / numbers_of_selections[i]
            # Radius of confidence interval
            delta_i = math.sqrt(3/2 * math.log(n + 1) / numbers_of_selections[i])
            # Upper bound
            upper_bound = average_reward + delta_i
        else:
            upper_bound = 1e400
        if upper_bound > max_upper_bound:   # new upper bound found
            max_upper_bound = upper_bound   # update
            ad = i                          # save corresp. ad
    ads_selected.append(ad)                 # ad selected for user
    numbers_of_selections[ad] += 1          # count selection for each ad
      
    reward = dataset.values[n, ad]
    sums_of_rewards[ad] += reward           # add reward to sum
    total_reward += reward                  # add reward to total reward

# Visualising the results
plt.hist(ads_selected)
plt.title('Histogram of ads selections')
plt.xlabel('Ads')
plt.ylabel('Number of times each ad was selected')
plt.show()